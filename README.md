INTRODUCTION
------------

This module allows to add metatag robots, 
if not present and will add indexation-controlling 
parameters <em>index,follow</em> to specific contact forms.

This will tell the search engines to index and 
will let the crawler to follow the links on selective 
forms only.


REQUIREMENTS
------------

No special requirements

INSTALLATION
------------

* Extract and enable this module.
* After enabling the module, each contact form will 
  show a checkbox "Enable Form Indexing".
* Select this checkbox to add indexing parameters 
  index,follow on this form.

CONFIGURATION
-------------

* "Enable Form Indexing" checkbox field value will be 
   used to enable indexing for this form.
